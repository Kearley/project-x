"""
Parses comma separated value (.csv) file into YAML format (.yaml) of the same name.
It takes a directory path and parses all .csv files within.

author: dkearley@integrationpartners.com

"""

########################################################################################
#
# Imports
#
########################################################################################

import csv
import sys
import yaml
 
########################################################################################
#
# Globals
#
########################################################################################

csv_data = []
csv_file_path = str(sys.argv[1])
csv_file_name = csv_file_path.split('/')[-1]
data_key = csv_file_name.split('.')[0]

########################################################################################
#
# Functions
#
########################################################################################

# Iterates through each .csv within a directory and renders a yaml file
def yaml_me():
    with open(csv_file_path) as x:
        reader = csv.DictReader(x)
        for row in reader:
            csv_data.append(row)
     
    with open(csv_file_path.split('.')[0] + '.yml', 'w') as outfile:
        outfile.write(yaml.dump({data_key: csv_data}, explicit_start=False, \
            default_flow_style=False))

########################################################################################
#
# Main
#
########################################################################################
if __name__ == "__main__":
    yaml_me()

