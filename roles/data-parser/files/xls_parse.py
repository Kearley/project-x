"""
Parses a single Excel spreadsheet (.xls) into separate comma separated value (.csv) file.
If more than one sheet in the spreadsheet workbook, a .csv file per sheet is generated.

author: dkearley@integrationpartners.com

"""

########################################################################################
#
# Imports
#
########################################################################################


from xlrd import open_workbook
import csv
import os
import argparse


########################################################################################
#
# Globals
#
########################################################################################



########################################################################################
#
# Functions
#
########################################################################################

# Iterates through each sheet in a .xls spreadsheet and converts to individual .cvs files
def data_parse(filename,out):
	wb = open_workbook(filename)
	out_dir = out + "/"
	for i in range(0,wb.nsheets):
		sheet = wb.sheet_by_index(i)
		# print(sheet.name)
		with open(str(out_dir) + "%s.csv" %(sheet.name.replace(" ","")), "w") as file:
			writer = csv.writer(file, delimiter = ",")
			# print (sheet, sheet.name, sheet.ncols,sheet.nrows)

			header = [cell.value for cell in sheet.row(0)]
			writer.writerow(header)

			for row_idx in range(1, sheet.nrows):
				row = [int(cell.value) if isinstance(cell.value, float) else cell.value for cell in sheet.row(row_idx)]
				writer.writerow(row)

########################################################################################
#
# Main
#
########################################################################################
if __name__ == "__main__":

# Collect user input and parse it for the spreadsheet parsing work
	parser = argparse.ArgumentParser(description="Parses an Excel spreadsheet into separate .csv files.")
	parser.add_argument('-f', '--filename', help='The spreadsheet file you want parsed.')
	parser.add_argument('-o', '--out', help='The output directory to write the .csv files to.')
	args = parser.parse_args()

	data_parse(args.filename,args.out)

