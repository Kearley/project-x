"""
Takes a user supplied network CIDR and subnet mask and calculates all valid subnets

author: dkearley@integrationpartners.com

"""

########################################################################################
#
# Imports
#
########################################################################################

from netaddr import *
import argparse

########################################################################################
#
# Globals
#
########################################################################################



########################################################################################
#
# Functions
#
########################################################################################

# Uses netaddr to calculate all subnets from a passed CIDR network and subnet mask
def gen_subnets(network,mask):
   my_network = IPNetwork(network)
   my_subnets = list(my_network.subnet(mask))
   return(my_subnets)


########################################################################################
#
# Main
#
########################################################################################

if __name__ == "__main__":

   # Collect user input and parse it for the subnet calculations
   parser = argparse.ArgumentParser(description="Generates all possible subnets for a given network.")
   parser.add_argument('-n', '--network', help='The base IP network as a CIDR for subnet allocations. \
      Example: 172.16.0.0/16 or 172.16.0.0/18')
   parser.add_argument('-m', '--mask', help='The bitmask used for the subnets. Example: 24')
   args = parser.parse_args()


   # Calculate the subnets
   my_subnets = gen_subnets(args.network,int(args.mask))

   for x in my_subnets:
      print(x)
   